#include <iostream>
#include <cstdlib>

using namespace std;

main(){
    //Unfinished!
    //random 2D room generator using section 5 and 6 stuff (arrays and loops)

    beginning:
    system("cls");

    char option;

    int width;
    int height;

    cout << "give room width:";
    cin >> width;
    cout << "give room height:";
    cin >> height;

    regenerate:
    system("cls");

    int pattern[height][width];

    int num = (width * height);
    int x = 0;
    int y = 0;

    cout << endl;
    cout << endl;
    while (num-- > 0){
        if(x == 0){
            pattern[y][x] = 0;
            cout << "|||";
            x++;
        }
        else if(x > 0 && x < (width - 1)){

            if(y > 0 && y < (height - 1)){
                pattern[y][x] = rand() % 5;
                if(pattern[y][x] == 0) cout << "|||";
                else if(pattern[y][x] > 0) cout << "   ";
            }
            else if(y == 0 || y == (height - 1)){
                pattern[y][x] = 0;
                cout << "|||";
            }
            x++;
        }
        else if(x == (width - 1)){
            pattern[y][x] = 0;
            cout << "|||" << endl;
            x = 0;
            y++;
        }
    }
    cout << endl;
    cout << "Generated " << width << "x" << height << " room." << endl;
    cout << endl;

    cout << "||| = wall" << endl;
    cout << "Generate new room with new width & height(y), same settings(r) or exit(any other key not y/r)?" << endl << ">>";
    cin >> option;
    
//    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    if(option == 'y') goto beginning;
    else if(option == 'r') goto regenerate;
    else if(option != 'n' && option != 'y') return 0;

}
