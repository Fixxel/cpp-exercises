#include <iostream>

using namespace std;

main(){
    //calculator exercise (section 4 mainly, using loop which is section 6 material)

    bool Done;
    double num1, num2, tnum;
    char choice = '0';

    while(!Done){

        if(choice == '0' && choice != 's'){
            cout << "Give num1" << endl;
            cin >> num1;

            cout << "Give num2" << endl;
            cin >> num2;
        }

        cout << "What would you like to do with the numbers?" << endl;
        cout << "0 - Reset numbers" << endl;
        cout << "+ - (" << num1 << " + " << num2 << " = num3)" << endl;
        cout << "- - (" << num1 << " - " << num2 << " = num3)" << endl;
        cout << "* - (" << num1 << " * " << num2 << " = num3)" << endl;
        cout << "/ - (" << num1 << " / " << num2 << " = num3)" << endl;
        cout << "s - switch num1 and num2" << endl;
        cout << "x - exit calculator" << endl;

        cin >> choice;

        switch(choice){
            case '0':
                cout << "Input new num1 and num2!" << endl;
                break;

            case '+':
                cout << num1 + num2 << endl;
                break;

            case '-':
                cout << num1 - num2 << endl;
                break;

            case '*':
                cout << num1 * num2 << endl;
                break;

            case '/':
                cout << num1 / num2 << endl;
                break;

            case 's':
                tnum = num1;
                num1 = num2;
                num2 = tnum;
                cout << "Numbers switched!" << endl;
                break;

            case 'x':
                cout << "Exiting" << endl;
                Done = true;
                return 0;
                break;

            default:
                cout << "Unidentified command." << endl;
                break;
        }
    }
}
