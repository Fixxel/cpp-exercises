#include <iostream>
#include <cstdlib>

using namespace std;

main(){
    //random 2D pattern generator using section 5 and 6 stuff (arrays and loops)
    beginning:
    system("cls");

    char option;

    int pattern[20][20];

    int num = 400;
    int x = 0;
    int y = 0;

    while (num > 0){    //creates pattern to 2 dimensional array 20x20
        pattern[y][x] = rand() % 3;

        if(x < 19) x++;
        else if(x >= 19){
            x = 0;
            y++;
        }

        num--;
    }

    x = 0;
    y = 0;
    num = 400;

    while (num > 0){    //couts the pattern from 2 dimensional array 20x20
        if(pattern[y][x] == 0) cout << "|G|";           //Ground
        else if(pattern[y][x] == 1) cout << "|W|";      //Water
        else if (pattern[y][x] == 2) cout << "|F|" ;    //Forest

        if(x < 19)x++;
        else if(x >= 19){
            x = 0;
            y++;
            cout << endl;
        }

        num--;
    }

    cout << "G = Ground, W = Water, F = Forest" << endl;
    cout << "Generate new? Y/N ";
    cin >> option;

    if(option == 'y' || 'Y') goto beginning;
    else if(option == ('n' || 'N')) exit(0);

}
